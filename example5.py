# Programa para convertir grados centigrados a farenheit, ejercicio 5
# autot =   Michael Vasquez
# Email =   "michael.vasquez@unl.edu.ec"
try:
    celsius  =   float(input("Ingresa la cantidad de grados centigrados\n  "))
    respuesta   =   (celsius * 9/5 ) + 32
    if celsius >= 37:
        print("Esta haciendo demasido calor")
    elif celsius >= 21:
        print("Esta haciendo un poco de calor")
    elif celsius >= 0:
        print("Esta haciendo un poco de frio")

    print("La temperatura en grados Farenheit es:", respuesta)

except ValueError:
    print("Ingrese numeros")




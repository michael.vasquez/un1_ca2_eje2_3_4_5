#   preguntar el nombre y dar una bienvenida
#   author  =   "Michael Vasquez"
#   email   =   michael.vasquez@unl.edu.ec

name = input(str("Ingresa tu nombre:\n "))
if name == 'jose':
    print("Hoy es tu santo", name)
elif name == 'milton':
    print("Felicidades has ganado ", name)
elif name == 'susana':
    print(name, " tienes una deuda que pagar")
elif name == 'danilo':
    print("Su casa fue hipotecada")
else:
    print("Hola", name)

